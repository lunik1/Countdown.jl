#!/usr/bin/env julia

include("../src/Countdown.jl")

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args

function main(ARGS)
    s = ArgParseSettings()

    @add_arg_table! s begin
        "--numbers", "-n"
        help =
            "The available numbers. If not set they will be generated " *
            "according to standard Countdown rules. Must be > 0."
        nargs = '+'
        arg_type = Int

        "--target", "-t"
        help =
            "Target number, if not set will be random in range 100-999. " *
            "Must be > 0."
        arg_type = Int
        range_tester = x -> x > 0

        "--all-solutions", "-A"
        help = "if set, print all possible solutions"
        action = :store_true

        "--redundant", "-r"
        help = "if set, do not try to remove redundant solutions"
        action = :store_true

        "--from-the-top", "-T"
        help =
            "When generating numbers, specifies the number to be taken" *
            "from the top. Must be in [0, 4]. Ignored if --numbers " *
            "is set. Random if not set."
        arg_type = Int
        range_tester = x -> 0 ≤ x ≤ 4
    end

    parsedArgs = parse_args(ARGS, s)

    # Generate numbers if they are not specified
    if isempty(parsedArgs["numbers"])
        numbers = Countdown.choose_numbers(
            parsedArgs["from-the-top"] === nothing ? rand(0:4) :
            parsedArgs["from-the-top"],
        )
        print("Generated n")
    else
        numbers = parsedArgs["numbers"]
        print("N")
    end
    println("umbers: ", numbers)

    # Generate a target if it is not specified
    if parsedArgs["target"] === nothing
        target = Countdown.generate_target()
        print("Generated t")
    else
        target = parsedArgs["target"]
        print("T")
    end
    println("arget: ", target)

    if parsedArgs["all-solutions"]
        solutions = Countdown.solve_numbers_all(
            numbers,
            target,
            prune=!parsedArgs["redundant"],
        )
        numSolutions = 0
        for solution in solutions
            println(Countdown.display(solution))
            numSolutions += 1
        end
        println(
            numSolutions,
            " solution",
            numSolutions == 1 ? "" : "s",
            " found",
        )
    else
        solution = Countdown.solve_numbers(
            numbers,
            target,
            closest=true,
            prune=!parsedArgs["redundant"],
        )

        println(
            solution.result ≠ target ?
            "No exact solution found, closest is:\n" *
            Countdown.display(solution) : Countdown.display(solution),
        )
    end
end

main(ARGS)

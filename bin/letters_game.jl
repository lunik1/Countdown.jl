#!/usr/bin/env julia

include("../src/Countdown.jl")

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Multisets: Multiset

function main(ARGS)
    s = ArgParseSettings()

    @add_arg_table! s begin
        "--letters", "-l"
        help =
            "the available letters, if not set they will be generated " *
            "according to standard Countdown rules"
        arg_type = String

        "--dictionaries", "-d"
        help =
            "Word dictionaries. Should be files containing a single " *
            "valid word on each line. If not set, the Moby project's " *
            "CROSSWD.TXT and CRSWD-D.TXT are used."
        nargs = '+'

        "--all-solutions", "-A"
        help =
            "If set, print all possible words. If not set, only the " *
            "longest possible words are given."
        action = :store_true

        "--vowels", "-v"
        help =
            "The number of vowels. Ignored if --letters is set. If " *
            "not set, will be a random number in [3, 5]."
        arg_type = Int
        range_tester = x -> x ≥ 0

        "--consonants", "-c"
        help =
            "The number of consonants. Ignored if --letters is set. " *
            "If not set, will be 9 - vowels."
        arg_type = Int
        range_tester = x -> x ≥ 0
    end

    parsedArgs = parse_args(ARGS, s)

    # Generate letters if they are not specified
    if parsedArgs["letters"] === nothing
        vowels =
            parsedArgs["vowels"] === nothing ? rand(3:5) : parsedArgs["vowels"]
        if parsedArgs["consonants"] === nothing
            letters = Countdown.choose_letters(vowels)
        else
            letters = Countdown.choose_letters(vowels, parsedArgs["consonants"])
        end
        print("Generated l")
    else
        letters = Multiset([uppercase(ch) for ch in parsedArgs["letters"]])
        print("L")
    end
    println("etters: ", letters)

    # Read in dictionaries
    DIR = @__DIR__
    dict = String[]
    if isempty(parsedArgs["dictionaries"])
        dict = vcat(
            Countdown.load_dict(DIR * "/../data/CROSSWD.TXT"),
            Countdown.load_dict(DIR * "/../data/CRSWD-D.TXT"),
        )
    else
        for path in parsedArgs["dictionaries"]
            dict = vcat(dict, Countdown.load_dict(path))
        end
    end

    # Sort dict from longest to shortest, subsort alphabetically, and remove
    # duplicates
    dict = sort!(unique(dict), alg=QuickSort, by=a -> (-length(a), a))

    words = Countdown.solve_letters(letters, dict)

    if parsedArgs["all-solutions"]
        numWords = 0
        for word in words
            println(word)
            numWords += 1
        end
        println(numWords, " word", numWords == 1 ? "" : "s", " found")
    else
        oldLength = 0
        for word in words
            len = length(word)
            if len ≥ oldLength
                println(word)
            else
                break
            end
            oldLength = len
        end
    end
end

main(ARGS)

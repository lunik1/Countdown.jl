
__precompile__()

module Countdown

export Token,
    Expression,
    display,
    choose_numbers,
    choose_letters,
    generate_target,
    load_dict,
    produce_totals,
    solve_numbers_all,
    solve_numbers,
    solve_letters

using Multisets: Multiset
using ResumableFunctions
using StatsBase: sample

"""
Allowed operations in the Countdown numbers game.
"""
const OPERATORS = Function[*, +, -, ÷]

"""
The possible members of the RPN stack representing a solution to the numbers
game, i.e. an Integer or any of the allowed operators.
"""
Token = Union{Integer, typeof.(OPERATORS)...}

"""
Represents an expression of integer arithmetic in reverse polish notation.
"""
struct Expression
    result::Int
    tokens::Vector{Token}
end

"""
    display(e::Expression)::String

Display a total as an equation with reverse Polish notation.
"""
function display(e::Expression)::String
    equation = ""
    for s in e.tokens
        equation *= s == div ? "÷ " : string(s) * " "
    end

    equation *= "= " * string(e.result)
end

"""
The standard set of large numbers on Countdown.
"""
const LARGE_NUMBERS = [25, 50, 75, 100]

"""
The standard set of small numbers on Countdown.
"""
const SMALL_NUMBERS =
    [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10]

"""
    choose_numbers(top::Integer)::Vector{Int}

Randomly take 6 numbers, taking `top` from `[25, 50, 75, 100]` numbers and the
remainder from `[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10]`.
"""
function choose_numbers(top::Integer)::Vector{Int}
    # Total number of number tiles in a standard game
    TOTAL_NUMBERS = 6

    if 0 ≤ top ≤ 4
        return vcat(
            sample(LARGE_NUMBERS, top, replace=false),
            sample(SMALL_NUMBERS, TOTAL_NUMBERS - top, replace=false),
        )
    else
        throw(ErrorException("Between 0 and 4 large numbers must be chosen"))
    end
end

#! format: off
"""
The vowel tiles used in Countdown.
"""
const VOWELS = ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A',
                'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E',
                'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
                'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O',
                'U', 'U', 'U', 'U', 'U']

"""
The consonant tiles used in Countdown
"""
const CONSONANTS = ['B', 'B',
                    'C', 'C', 'C',
                    'D', 'D', 'D', 'D', 'D', 'D',
                    'F', 'F',
                    'G', 'G', 'G',
                    'H', 'H',
                    'J',
                    'K',
                    'L', 'L', 'L', 'L', 'L',
                    'M', 'M', 'M', 'M',
                    'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
                    'P', 'P', 'P', 'P',
                    'Q',
                    'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R',
                    'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S',
                    'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T',
                    'V',
                    'W',
                    'X',
                    'Y',
                    'Z']
#! format: on

"""
    choose_letters(vowels::Integer, consonants::Integer)::Multiset{Char}

Randomly select `vowels` vowels and `consonants` consonants from the
distribution of letters given by http://www.thecountdownpage.com/letters.htm
"""
function choose_letters(vowels::Integer, consonants::Integer)::Multiset{Char}
    if vowels < 0 || vowels > length(VOWELS)
        throw(
            ErrorException(
                "Between 0 and $(length(VOWELS)) " * "vowels must be chosen",
            ),
        )
    end
    if consonants < 0 || consonants > length(CONSONANTS)
        throw(
            ErrorException(
                "Between 0 and $(length(CONSONANTS)) " *
                "consonants must be chosen",
            ),
        )
    end

    Multiset(
        vcat(
            sample(VOWELS, vowels, replace=false),
            sample(CONSONANTS, consonants, replace=false),
        ),
    )
end

"""
    choose_letters(vowels::Integer)::Multiset{Char}

Randomly select 9 letters, `vowels` of which are vowels. The distribution of
letters is taken from http://www.thecountdownpage.com/letters.htm
"""
function choose_letters(vowels::Integer)::Multiset{Char}
    # total number of tiles in a standard Countdown game
    TOTAL_LETTERS = 9

    return choose_letters(vowels, TOTAL_LETTERS - vowels)
end

"""
    generate_target()::Int

Generate a random number between 100 and 999.
"""
function generate_target()::Int
    return rand(100:999)
end

"""
    load_dict(path::AbstractString)::Vector{String}

Load dict of words in file `path` into a set. The file should contain a list of
words, each on a new line. Entries will be CAPITALISED on import.
"""
function load_dict(path::AbstractString)::Vector{String}
    s = nothing
    open(path) do f
        s = Vector{String}(map(uppercase, readlines(f)))
    end

    return s
end

"""
    addinorder(trail::AbstractVector{Token})::Bool

Checks if pushing a `+` to the stack would give a sequence of +s and -s in the
canonical order.

There are many redundant ways of writing sequences of +/- and */÷ operations:
e.g. 4 3 2 1 + + - = 2 4 + 3 + 1 - = 4 1 - 2 3 + + etc.
so to reduce this when looking for solutions to the numbers game, a specific
"canonical" order of these operations is enforced: -s and ÷s last, descending
operand size. In the above example, 4 3 + 2 + 1 -.

See also: [`addinorder`](@ref), [`numbers_dfs`](@ref)
"""
@inline function addinorder(trail::AbstractVector{Token})::Bool
    if length(trail) < 3
        return true
    end

    # we are either x y + z or w x + y + z

    @inbounds y = trail[end - 2]
    @inbounds last_op = trail[end - 1]
    @inbounds z = trail[end]

    !(
        y isa Integer &&
        z isa Integer &&
        (last_op isa typeof(+) && y < z || last_op isa typeof(-))
    )
end

"""
    mulinorder(trail::AbstractVector{Token})::Bool

Checks if pushing a `*` to the stack would give a sequence of *s and ÷s in the
canonical order.

For more information on "canonical order", see [`addinorder`](@ref).

See also: [`addinorder`](@ref), [`numbers_dfs`](@ref)
"""
function mulinorder end

@inline function mulinorder(trail::AbstractVector{Token})::Bool
    if length(trail) < 3
        return true
    end

    @inbounds y = trail[end - 2]
    @inbounds last_op = trail[end - 1]
    @inbounds z = trail[end]

    !(
        y isa Integer &&
        z isa Integer &&
        (last_op isa typeof(*) && y < z || last_op isa typeof(÷))
    )
end

"""
    numbers_dfs(
        stack::AbstractVector{<:Integer},
        trail::AbstractVector{Token},
        bag::Multiset{<:Int};
        prune::Bool=true)::Expression

Resumable function. Internal implementation of DFS for numbers game solutions.

Performs a depth-first search for complete RPN expressions using OPERATORS and
the numbers in `bag`. `stack` is the numbers currently on the calculation stack
and `trail` contains the numbers/operators visited so far (in least to most
recent order).

If `prune` is true (the default) then some heuristics are enabled to avoid
redundant and trivial solutions, these are (for an infix operation
"lhs ∘ rhs"):
• Only allowing operations where lhs ≥ rhs
• Disallowing lhs * rhs and lhs ÷ rhs where rhs = 1
• Disallowing lhs * rhs where lhs = 1
• Disallowing any intermediate value of 0
• Removing some* results which are redundant because of the associative and
commutative properties of +, -, *, and ÷ (see [`addinorder`](@ref)).

*still a WIP

See also: [`produce_totals`](@ref)
"""
function numbers_dfs end

@resumable function numbers_dfs(
    stack::AbstractVector{<:Integer},
    trail::AbstractVector{Token},
    bag::Multiset{<:Int};
    prune::Bool=true,
)::Expression
    # trail is a valid RPN expression
    @inbounds if length(stack) == 1
        @yield Expression(stack[1], deepcopy(trail))
    end

    # Recursive steps
    # Push operator
    if length(stack) ≥ 2
        for op in OPERATORS
            rhs = pop!(stack)
            lhs = pop!(stack)

            @inbounds last = trail[end]

            # Enforce rules:
            #  • no negatives
            #  • no fractions
            #  • avoid dividng by zero
            valid =
                (op ≠ (-) || lhs ≥ rhs) &&
                (op ≠ (÷) || rhs ≠ 0 && lhs % rhs == 0)

            # Tree pruning
            #  • lhs ≥ rhs skips redundant associative pairs for + and *
            #  • lhs - rhs = 0 is redundant (-ves handled by above rule)
            #  • rhs or lhs == 1 under multiplication is redundant
            #  • lhs % rhs ≠ 0 is disallowed when dividing (no fractions)
            #  • rhs == 1 under division is redundant
            #  • + or - after + redundant (associativity)
            #  • + after - is redundant
            #  • * or ÷ after * or ÷ is redundant (associativity)
            #  • see mulinorder()/addinorder() for operation ordering rules
            #! format: off
            unpruned = !prune ||
                (lhs ≥ rhs &&
                 (op ≠ (+) || last ≠ (+) && last ≠ (-) && addinorder(trail)) &&
                 (op ≠ (*) || rhs ≠ 1 && lhs ≠ 1 && last ≠ (*) && last ≠ (÷) && mulinorder(trail)) &&
                 (op ≠ (-) || rhs ≠ lhs && last ≠ (+)) &&
                 (op ≠ (÷) || rhs ≠ 1 && last ≠ (*) && last ≠ (÷)))
            #! format: on

            if valid && unpruned
                res = op(lhs, rhs)

                for s in numbers_dfs(
                    push!(stack, res),
                    push!(trail, op),
                    bag,
                    prune=prune,
                )
                    @yield s
                end

                # Reset
                pop!(trail)
                pop!(stack)
            end

            # Reset
            push!(stack, lhs)
            push!(stack, rhs)
        end
    end

    # Push number
    for num in unique(bag)
        for s in numbers_dfs(
            push!(stack, num),
            push!(trail, num),
            push!(bag, num, -1),
            prune=prune,
        )
            @yield s
        end

        # Reset
        pop!(stack)
        pop!(trail)
        push!(bag, num, 1)
    end
end

"""
    produce_totals(numbers::AbstractVector{<:Integer};
        prune::Bool=true)::Expression

Produce valid totals for every possible combination of `numbers` with the
allowed operators.

A total is valid if it is never negative or non-integer at any intermediate
stage. Enabling `prune` will enable the removal of redundant solutions, see
[`numbers_dfs`](@ref) for more info.
"""
function produce_totals end

@resumable function produce_totals(
    numbers::AbstractVector{<:Integer};
    prune::Bool=true,
)::Expression
    nnum = length(numbers)

    for eq in numbers_dfs(
        sizehint!(Int[], nnum),
        sizehint!(Token[], 2nnum + 1),
        Multiset(numbers),
        prune=prune,
    )
        @yield eq
    end
end

"""
    solve_numbers(numbers::AbstractVector{<:Integer},
                  target::Integer;
                  closest::Bool=false,
                  prune::Bool=true)::Expression

Return a total which represents a solution of the Countdown numbers game for
the given `numbers` and `target`.

If `closest` is set and no exact solution is found, the closest solution is
returned.

Enabling `prune` will enable the removal of redundant solutions, see
[`numbers_dfs`](@ref) for more info.

# Examples

```jldoctest
julia> solve_numbers([1, 2, 3, 4, 5, 100], 999)
total(999,Integer[2,5,100,1],(*,*,-))
```

This total represents the solution 2 * 5 * 100 - 1 = 999
"""
function solve_numbers(
    numbers::AbstractVector{<:Integer},
    target::Integer;
    closest::Bool=false,
    prune::Bool=true,
)::Expression
    closest_total = Expression(0, [])

    for total in produce_totals(numbers, prune=prune)
        if total.result == target
            return total
        elseif closest &&
               abs(total.result - target) < abs(closest_total.result - target)
            closest_total = total
        end
    end

    if closest
        return closest_total
    end
end

"""
    solve_numbers_all(numbers::AbstractVector{<:Integer},
                      target::Integer;
                      prune::Bool=true)::Expression

Generate totals which represent solutions of the Countdown numbers game for
the given `numbers` and `target`.

Enabling `prune` will enable the removal of redundant solutions, see
[`numbers_dfs`](@ref) for more info.
"""
function solve_numbers_all end

@resumable function solve_numbers_all(
    numbers::AbstractVector{<:Integer},
    target::Integer;
    prune::Bool=true,
)::Expression
    for total in produce_totals(numbers, prune=prune)
        if total.result == target
            @yield total
        end
    end
end

"""
    solve_letters(
        letters::Multiset{<:AbstractChar},
        dict::AbstractVector{T},
    )::T where {T<:AbstractString}

Resumable function. Yields words in `dict` which can be formed from the letters
in `letters`.

Possible words will be produced in their order in dict.
"""
function solve_letters end

@resumable function solve_letters(
    letters::Multiset{<:AbstractChar},
    dict::AbstractVector{T},
)::T where {T<:AbstractString}
    for word in dict
        if issubset(Multiset([ch for ch in word]), letters)
            @yield word
        end
    end
end

end
